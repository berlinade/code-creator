# generated euler-root system function

import numpy as np


sin = np.sin


def create_euler_sys(_, x_old, t_new, h):
    phi_old, omega_old = x_old
    def sys_func(x):
        phi, omega = x
        return np.array([omega - ((phi - phi_old)/h), (9.81/0.8)*sin(phi) + ((omega - omega_old)/h)])
    
    return sys_func
