import numpy as np

from scipy.optimize import root

from code_creator import Atom
from mathematical_pendulum.system_function_classic import sys_func_classic

import matplotlib
import matplotlib.pyplot as plt

from typing import Callable, Optional


sin = np.sin


def euler_generate() -> None:
    t = Atom.var('t')

    x = Atom.var('x')
    d_x = Atom.var('d_x')

    phi = Atom.var('phi')
    d_phi = Atom.var('d_phi')

    omega = Atom.var('omega')
    d_omega = Atom.var('d_omega')

    g, length = 9.81, Atom.ensure(0.8)

    out_0: Atom = omega - d_phi
    out_1: Atom = (g/length)*sin(phi) + d_omega

    sys_func: Atom = Atom.var('sys_func')

    sys_func_str: str = f'''
def {sys_func(d_x, x, t)}:
    {d_phi}, {d_omega} = {d_x}
    {phi}, {omega} = {x}
    return np.array([{out_0}, {out_1}])
'''

    print(sys_func_str)

    # =====================================================================

    phi_old = Atom.var('phi_old')
    omega_old = Atom.var('omega_old')

    h = Atom.var('h')
    d_phi.deps = (div_diff(phi, phi_old, h),)
    d_phi.use_identifier = False
    d_omega.deps = (div_diff(omega, omega_old, h),)
    d_omega.use_identifier = False

    sys_func_str_with_backed_in_euler: str = f'''
def {sys_func(x)}:
    {phi}, {omega} = {x}
    return np.array([{out_0}, {out_1}])
'''

    print(sys_func_str_with_backed_in_euler)

    _tmp = "\n    ".join(sys_func_str_with_backed_in_euler[1:].split('\n'))
    euler_sys_func: str = f'''
# generated euler-root system function

import numpy as np


sin = np.sin


def create_euler_sys(_, x_old, t_new, {h}):
    {phi_old}, {omega_old} = x_old
    {_tmp}
    return sys_func
'''

    print(euler_sys_func)
    with open('system_function.py', mode = 'w') as out_stream:
        out_stream.write(euler_sys_func[1:])


def div_diff(v_new: np.ndarray, v_old: np.ndarray, h: float):
    return (v_new - v_old)/h


def create_euler_sys_classic(sys_func: Callable[[np.ndarray, np.ndarray, float], np.ndarray],
                             x_old: np.ndarray,
                             t_new: float,
                             h: float) -> Callable[[np.ndarray], np.ndarray]:
    def euler_sys(x_new): return sys_func(div_diff(x_new, x_old, h), x_new, t_new)
    return euler_sys


def plot_sol(Ts: list[float], Xs: list[np.ndarray], title: str) -> None:
    fig, ax = plt.subplots()
    ax.plot(Ts, Xs)

    ax.set(xlabel = 'time (s)', ylabel = 'angle/angle_speed',
           title = title)
    ax.grid()


def simulate(create_euler_sys: Callable,
             sys_func: Optional[Callable] = None) -> tuple[list[float],
                                                           list[np.ndarray]]:
    t0: float = 0.0  # [s], initial time
    T: float = 15.0  # [s], terminal time
    hh: float = 0.05  # [s], general step-width
    x0: np.ndarray = np.array([np.pi/2.0, 0.0])  # phi_0, omega_0

    Ts: list[float] = [t0]
    Xs: list[np.ndarray] = [x0]

    print_event: float = 0.0  # [s]
    while Ts[-1] < T - hh/2:
        Ts.append(min(T, Ts[-1] + hh))
        h = Ts[-1] - Ts[-2]  # actual step-width

        euler_sys = create_euler_sys(sys_func, Xs[-1], Ts[-1], h)
        report = root(euler_sys, x0 = Xs[-1])

        perc: float = (Ts[-1] - t0)/(T - t0)  # [1]
        if perc >= print_event:
            print(f'{100.0*perc:.3f}%; root.report.success: {report.success}')
            print_event += 0.1
        Xs.append(report.x)
    print('finished simulation')

    return Ts, Xs


def main_classic():
    Ts, Xs = simulate(create_euler_sys_classic, sys_func_classic)
    plot_sol(Ts, Xs, 'math pendulum - computed "classically"')


def main_from_compiling():
    euler_generate()

    from mathematical_pendulum.system_function import create_euler_sys

    Ts, Xs = simulate(create_euler_sys, None)
    plot_sol(Ts, Xs, 'math pendulum - computed via (cc)')


if __name__ == '__main__':
    main_classic()
    main_from_compiling()

    plt.show()
