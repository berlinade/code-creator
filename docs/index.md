# code-creator (cc)

## (A Short) Description

A package to generate python-code at python-run-time

can be used for:

- generation of large mathematical models
- dynamization of python code
- resolving function chains
- embedding into algorithmic differentiation

## Getting Started

### Installation

... via pip from PYPI (python package index)
```bash
pip install code-creator
```

... or locally (using pip)
```bash
git clone https://gitlab.com/berlinade/code-creator.git
cd code-creator
pip install -e .
```

### Usage

... *under construction*

## Documentation

... can be found here (*under construction*): [https://berlinade.gitlab.io/code-creator/](https://berlinade.gitlab.io/code-creator/)

## Find code-creator

- @ GitLab: [https://gitlab.com/berlinade/code-creator](https://gitlab.com/berlinade/code-creator)
- @ PYPI: [https://pypi.org/project/code-creator/](https://pypi.org/project/code-creator/)
- author: [codima on youtube.com](https://www.youtube.com/channel/UCwnthITQqkWgaHnz82U7WsA)

## (Informal Info on) License

this version of the "polynom" has been published under GPLv3 (see [LICENSE](https://gitlab.com/berlinade/code-creator/-/blob/main/LICENSE)). <br>
Alternative Licenses are negotiable.
