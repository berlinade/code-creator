
▸ Ops
-----
**declaration**

```python
class Ops(object): 
```



-----
▸ Atom
-----
**declaration**

```python
class Atom(object): 
```



??? abstract "member functions"

    ▹ Atom.identity

    ▹ Atom.var

    ▹ Atom.const

    ▹ Atom.ensure

    ▹ Atom.\_\_init\_\_

    ▹ Atom.identifier

    ▹ Atom.identifier

    ▹ Atom.paren\_group

    ▹ Atom.str

    ▹ Atom.\_\_str\_\_

    ▹ Atom.\_\_pos\_\_

    ▹ Atom.\_\_add\_\_

    ▹ Atom.\_\_radd\_\_

    ▹ Atom.\_\_neg\_\_

    ▹ Atom.\_\_sub\_\_

    ▹ Atom.\_\_rsub\_\_

    ▹ Atom.\_\_mul\_\_

    ▹ Atom.\_\_rmul\_\_

    ▹ Atom.\_\_truediv\_\_

    ▹ Atom.\_\_rtruediv\_\_

    ▹ Atom.\_\_floordiv\_\_

    ▹ Atom.\_\_rfloordiv\_\_

    ▹ Atom.\_\_pow\_\_

    ▹ Atom.\_\_rpow\_\_

    ▹ Atom.abspow

    ▹ Atom.rabspow

    ▹ Atom.sin

    ▹ Atom.cos

    ▹ Atom.tan

    ▹ Atom.arctan2

    ▹ Atom.atan2

    ▹ Atom.rarctan2

    ▹ Atom.ratan2

    ▹ Atom.exp

    ▹ Atom.log

    ▹ Atom.\_\_abs\_\_

    ▹ Atom.relu

    ▹ Atom.maximum

    ▹ Atom.max

    ▹ Atom.minimum

    ▹ Atom.min

    ▹ Atom.\_\_getitem\_\_

    ▹ Atom.\_\_call\_\_

    ▹ Atom.attrib

    ▹ Atom.assign

    ▹ Atom.tupl\_unload

    ▹ Atom.dict\_unload

-----
▹ Atom.identity
-----
**declaration**

```python
@classmethod 
def identity(cls, arg: Union[Self, int, float, bool, str], callback: Callable[[Self], None] | None = None) -> Self: 
```



-----
▹ Atom.var
-----
**declaration**

```python
@classmethod 
def var(cls, identifier: str | None = None, callback: Callable[[Self], None] | None = None) -> Self: 
```



-----
▹ Atom.const
-----
**declaration**

```python
@classmethod 
def const(cls, val: int | float | bool | str, callback: Callable[[Self], None] | None = None): 
```



-----
▹ Atom.ensure
-----
**declaration**

```python
@classmethod 
def ensure(cls, other: Union[Self, int, float, bool, str], callback: Callable[[Self], None] | None = None) -> Self: 
```



-----
▹ Atom.\_\_init\_\_
-----
**declaration**

```python
def __init__(self, op: str, deps: tuple[Self, ...], val: int | float | bool | str | None = None, identifier: str | None = None, callback: Callable[[Self], None] | None = None): 
```



-----
▹ Atom.identifier
-----
**declaration**

```python
@property 
def identifier(self) -> str: return self._identifier 
```



-----
▹ Atom.identifier
-----
**declaration**

```python
@identifier.setter 
def identifier(self, new_ident: str | None) -> None: 
```



-----
▹ Atom.paren\_group
-----
**declaration**

```python
@property 
def paren_group(self) -> int: 
```


  paren_group (parentheses group)
  0 - bivariate linear (+ [binary], - [binary])
  1 - univariate linear (- [unary])
  2 - product (*)
  3 - division (/, //) (special rule: 0, 2, 3 require parenthesis)  # note: 0, 2 not strictly necessary here!
  4 - (default) -or- nonlinear operations (var, const, sin, abs, ...) (special rule: no parenthesis at all)
  5 - other bivariate ops (%, **) (special rule: 5 require parenthesis)



-----
▹ Atom.str
-----
**declaration**

```python
def str(self, outer_paren_group: int | None = None, use_identifier: bool | None = None) -> str: 
```



-----
▹ Atom.\_\_str\_\_
-----
**declaration**

```python
def __str__(self) -> str: 
```



-----
▹ Atom.\_\_pos\_\_
-----
**declaration**

```python
def __pos__(self) -> Self: 
```



-----
▹ Atom.\_\_add\_\_
-----
**declaration**

```python
def __add__(self, other: Union[Self, int, float, bool, str]) -> Self: 
```



-----
▹ Atom.\_\_radd\_\_
-----
**declaration**

```python
def __radd__(self, other: int | float | bool) -> Self: 
```



-----
▹ Atom.\_\_neg\_\_
-----
**declaration**

```python
def __neg__(self) -> Self: 
```



-----
▹ Atom.\_\_sub\_\_
-----
**declaration**

```python
def __sub__(self, other: Union[Self, int, float, bool, str]) -> Self: 
```



-----
▹ Atom.\_\_rsub\_\_
-----
**declaration**

```python
def __rsub__(self, other: int | float | bool) -> Self: 
```



-----
▹ Atom.\_\_mul\_\_
-----
**declaration**

```python
def __mul__(self, other: Union[Self, int, float, bool, str]) -> Self: 
```



-----
▹ Atom.\_\_rmul\_\_
-----
**declaration**

```python
def __rmul__(self, other: int | float | bool) -> Self: 
```



-----
▹ Atom.\_\_truediv\_\_
-----
**declaration**

```python
def __truediv__(self, other: Union[Self, int, float, bool, str]) -> Self: 
```



-----
▹ Atom.\_\_rtruediv\_\_
-----
**declaration**

```python
def __rtruediv__(self, other: int | float | bool) -> Self: 
```



-----
▹ Atom.\_\_floordiv\_\_
-----
**declaration**

```python
def __floordiv__(self, other: Union[Self, int, float, bool, str]) -> Self: 
```



-----
▹ Atom.\_\_rfloordiv\_\_
-----
**declaration**

```python
def __rfloordiv__(self, other: int | float | bool) -> Self: 
```



-----
▹ Atom.\_\_pow\_\_
-----
**declaration**

```python
def __pow__(self, other: Union[Self, int, float, bool, str]) -> Self: 
```



-----
▹ Atom.\_\_rpow\_\_
-----
**declaration**

```python
def __rpow__(self, other: int | float | bool) -> Self: 
```



-----
▹ Atom.abspow
-----
**declaration**

```python
def abspow(self, other: Union[Self, int, float, bool, str] = 2.0): 
```



-----
▹ Atom.rabspow
-----
**declaration**

```python
def rabspow(self, other: int | float | bool): 
```



-----
▹ Atom.sin
-----
**declaration**

```python
def sin(self): 
```



-----
▹ Atom.cos
-----
**declaration**

```python
def cos(self): 
```



-----
▹ Atom.tan
-----
**declaration**

```python
def tan(self): 
```



-----
▹ Atom.arctan2
-----
**declaration**

```python
def arctan2(self, other: Union[Self, int, float, bool, str]): 
```



-----
▹ Atom.atan2
-----
**declaration**

```python
def atan2(self,*args,**kwargs): 
```



-----
▹ Atom.rarctan2
-----
**declaration**

```python
def rarctan2(self, other: int | float | bool): 
```



-----
▹ Atom.ratan2
-----
**declaration**

```python
def ratan2(self,*args,**kwargs): 
```



-----
▹ Atom.exp
-----
**declaration**

```python
def exp(self): 
```



-----
▹ Atom.log
-----
**declaration**

```python
def log(self): 
```



-----
▹ Atom.\_\_abs\_\_
-----
**declaration**

```python
def __abs__(self): 
```



-----
▹ Atom.relu
-----
**declaration**

```python
def relu(self): 
```



-----
▹ Atom.maximum
-----
**declaration**

```python
def maximum(self,*others: Union[Self, int, float, bool, str]): 
```



-----
▹ Atom.max
-----
**declaration**

```python
def max(self,*args): 
```



-----
▹ Atom.minimum
-----
**declaration**

```python
def minimum(self,*others: Union[Self, int, float, bool, str]): 
```



-----
▹ Atom.min
-----
**declaration**

```python
def min(self,*args): 
```



-----
▹ Atom.\_\_getitem\_\_
-----
**declaration**

```python
def __getitem__(self,*items: Union[Self, int, float, bool, str]): 
```



-----
▹ Atom.\_\_call\_\_
-----
**declaration**

```python
def __call__(self,*args: Union[Self, int, float, bool, str]): 
```



-----
▹ Atom.attrib
-----
**declaration**

```python
def attrib(self, attr: Union[Self, int, float, bool, str]): 
```



-----
▹ Atom.assign
-----
**declaration**

```python
def assign(self, other: Union[Self, int, float, bool, str]): 
```



-----
▹ Atom.tupl\_unload
-----
**declaration**

```python
def tupl_unload(self): 
```



-----
▹ Atom.dict\_unload
-----
**declaration**

```python
def dict_unload(self): 
```



-----
▸ ExpressionGraph
-----
**declaration**

```python
class ExpressionGraph(object): 
```



??? abstract "member functions"

    ▹ ExpressionGraph.\_\_init\_\_

    ▹ ExpressionGraph.callback

-----
▹ ExpressionGraph.\_\_init\_\_
-----
**declaration**

```python
def __init__(self): 
```



-----
▹ ExpressionGraph.callback
-----
**declaration**

```python
def callback(self, op: Atom): 
```



-----