
▸ Symbol
-----
**declaration**

```python
class Symbol(object): 
```



??? abstract "member functions"

    ▹ Symbol.\_\_init\_\_

    ▹ Symbol.\_\_del\_\_

    ▹ Symbol.exists

    ▹ Symbol.get

    ▹ Symbol.\_\_str\_\_

    ▹ Symbol.d

-----
▹ Symbol.\_\_init\_\_
-----
**declaration**

```python
def __init__(self, base_sym: symbol_t, comment: str | None = None): 
```



-----
▹ Symbol.\_\_del\_\_
-----
**declaration**

```python
def __del__(self) -> None: 
```



-----
▹ Symbol.exists
-----
**declaration**

```python
@classmethod 
def exists(cls, item: symbol_t) -> bool: return item in cls._all_sym_chars 
```



-----
▹ Symbol.get
-----
**declaration**

```python
@classmethod 
def get(cls, item: symbol_t) -> Union['Symbol', None]: return cls._sym_reg.get(item, None) 
```



-----
▹ Symbol.\_\_str\_\_
-----
**declaration**

```python
def __str__(self) -> str: return self.base_sym 
```



-----
▹ Symbol.d
-----
**declaration**

```python
def d(self,*dargs: symbol_degree_t) -> str: 
```



-----