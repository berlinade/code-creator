
  sin
-----
**declaration**

```python
def sin(x,*args,**kwargs): 
```


forward alias to numpy.sin



-----
  cos
-----
**declaration**

```python
def cos(x,*args,**kwargs): 
```


forward alias to numpy.cos



-----
  exp
-----
**declaration**

```python
def exp(x,*args,**kwargs): 
```


forward alias to numpy.exp



-----
  log
-----
**declaration**

```python
def log(x,*args,**kwargs): 
```


forward alias to numpy.log



-----
  tan
-----
**declaration**

```python
def tan(x,*args,**kwargs): 
```


forward alias to numpy.tan



-----
  abspow
-----
**declaration**

```python
def abspow(base: float | Any, power: float | int | Any = 2.0): 
```



-----
  arctan2
-----
**declaration**

```python
def arctan2(x1: float | Any, x2: float | Any): 
```



-----
  relu
-----
**declaration**

```python
def relu(arg: float | Any): 
```



-----
  maximum
-----
**declaration**

```python
def maximum(*others: float | Any): 
```



-----
  minimum
-----
**declaration**

```python
def minimum(*others: float | Any): 
```



-----