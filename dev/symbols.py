from code_creator.symbols import Symbol


def dev_symbol():
    q = Symbol('q', 'pipe_01')
    print(Symbol.get('test'), Symbol.exists('test'))  # 'test' does not appear in symbol-list
    print(Symbol.get('q'), Symbol.exists('q'))
    print(q.d(('t', 1), ('x', 2), ('y', 0), ('z', -1)))
    print(q.d(('x', 0)))
    print(q.d())


if __name__ == '__main__': dev_symbol()
