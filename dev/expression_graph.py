import numpy as np

from code_creator import Atom
from code_creator.math import sin, abspow, relu, maximum


def dev_atom():
    def callback(arg: Atom) -> None: pass  # print(arg.identifier)

    # x = Atom(Ops.var, tuple())
    # x.identifier = 'x'
    # x.use_identifier = True
    x = Atom.var('x', callback = callback)  # , sym = 'x', tag = 'pipe_01')

    y = Atom.var(callback = callback)

    term = (x[3.0] + y[x]/x + 3.1)/x

    try: print(term)
    except AssertionError:
        print('set y identifier name!')
        y.identifier = 'y'
        print(term)
        print(term[17.0])
    print((abspow(term) + 4.01*y + sin(-(-x - y)))/(x*y + x*(y + x)))
    print(term2 := abspow(2.0, term))

    y.deps = (x - 2.01*x,)
    print(y)
    y.use_identifier = False
    print(y)
    print(term2)
    y.use_identifier = True
    print(y)
    print(term2)

    print(sin((x**y)**x)**(x - y) + 17.01)

    print(relu(x), x.relu())
    print(term3 := maximum(x, y, y, term, x, x, y, term2, y, y, 3.0*x))
    print(term3(x, y))

    print(x.assign(term3))
    try: print(x.assign(term3).assign(term3))
    except: print(x(np.pi, y.assign(99), sin(x).tupl_unload(), x.tupl_unload(), term.dict_unload()))

    print(sin(x + '*test'))

    print(x.attrib('test'))
    print(x.attrib(y))
    print(x.attrib(term))
    print(term.attrib(x))
    print(term.attrib(term))


if __name__ == '__main__': dev_atom()
